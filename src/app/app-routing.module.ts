import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {BlogComponent} from "./components/blog/blog.component";
import {AddTeamComponent} from "./components/cards/add-team/add-team.component";
import {TeamComponent} from "./components/cards/team/team.component";
import {AddPlayerComponent} from "./components/cards/add-player/add-player.component";
import {ScheduleComponent} from "./components/schedule/schedule.component";
import { AddEventComponent } from './components/cards/add-event/add-event.component';
import {EditStatisticComponent} from "./components/cards/edit-statistic/edit-statistic.component";
import {LoginComponent} from "./components/login/login.component";
import {AuthGuardService} from "./service/guards/auth-guard.service";
import {SignupComponent} from "./components/signup/signup.component";


const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'schedule',
    component: ScheduleComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'blog',
    component: BlogComponent,
    canActivate: [AuthGuardService]

  },
  {
    path: 'addTeam',
    component: AddTeamComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'team',
    component: TeamComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'addPlayer',
    component: AddPlayerComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'addEvent',
    component: AddEventComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'editStatistic',
    component: EditStatisticComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'signup',
    component: SignupComponent
  }];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})



export class AppRoutingModule { }
