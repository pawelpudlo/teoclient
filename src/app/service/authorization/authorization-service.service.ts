import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {SignInData} from "../../components/login/sign-in-data/signInData";
import {SharedService} from "../../shared/shared.service";
import {Router} from "@angular/router";

export const LOGIN_STATUS_OK = 'ok';
export const LOGIN_STATUS_UNAUTHORIZED = 'forbidden';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationServiceService {


  basePath: string = 'http://localhost:8080';
  code: string = "";


  constructor(public httpClient: HttpClient,public sharedService: SharedService,private router: Router) { }

  authenticate(login: string, password: string): boolean {

    this.tryLogin(login,password).subscribe(response =>{
      console.log(response.information)
      this.code = response.information;
      this.sharedService.setKey(this.code);
      localStorage.setItem('key', response.information);
      localStorage.setItem('status', LOGIN_STATUS_OK);
      this.router.navigate(['/blog']);
    }, error => {
      console.log(error);
      localStorage.setItem('status', LOGIN_STATUS_UNAUTHORIZED);
    })

    return true;
  }

  tryLogin(login: string, pass: string){
    return this.httpClient.post<information>(this.basePath + "/logIn",{"login": login, "password": pass});
  }

  registration(login: string, pass: string){
    return this.httpClient.post(this.basePath + "/registerAccount",{"login": login, "password": pass});
  }

  isAuthenticated(): boolean {
    const loginStatus = localStorage.getItem('status');

    return !!loginStatus && loginStatus === LOGIN_STATUS_OK;
  }

  logout(): void{
    localStorage.removeItem('key')
    localStorage.removeItem('status')
  }

}


export interface information {

  information: string;
}
