import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {SignInData} from "../components/login/sign-in-data/signInData";
import {SharedService} from "../shared/shared.service";

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  basePath: string = 'http://localhost:8080/api';
  headers_object!: HttpHeaders;

  constructor(private httpClient: HttpClient,public shared: SharedService) {
    console.log(shared.getKey())


    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('key')
    })
  }



  public getTeams(): Observable<Team[]> {

    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('key')
    })
    return this.httpClient.get<Team[]>(this.basePath + "/getAllTeams",{headers: this.headers_object });
  }

  public addTeam(teamName: string){
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('key')
    })
    console.log(this.basePath + '/addTeam/' + teamName)
    return this.httpClient.post(this.basePath + '/addTeam/' + teamName,{},{headers: this.headers_object });
  }

  public addPlayer(name: string, familyName: string, age: string,idTeam: number){
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('key')
    })
    let nameLink: string = "?name=" + name;
    let familyNameLink: string = "&familyname=" + familyName;
    let ageLink: string = "&age=" + age;
    let idTeamLink: string = "&idTeam=" + idTeam;


    console.log(this.basePath + '/addTeam' + nameLink + familyNameLink + ageLink + idTeamLink)
    return this.httpClient.post(this.basePath + '/addPlayer' + nameLink + familyNameLink + ageLink + idTeamLink,{},{headers: this.headers_object });
  }

  public getAllPlayersByTeam(idTeam: number): Observable<Players[]>{
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('key')
    })
    return this.httpClient.get<Players[]>(this.basePath + "/getAllPlayersByTeam/" + idTeam,{headers: this.headers_object },);
  }

  public getAllScheduleByIdTeam(idTeam: number): Observable<Schedule[]>{
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('key')
    })
    return this.httpClient.get<Schedule[]>(this.basePath + "/getAllSchedulesByTeam/" + idTeam,{headers: this.headers_object });
  }

  public getAllNameEvents(): Observable<EventsName[]> {
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('key')
    })
    return this.httpClient.get<EventsName[]>(this.basePath + "/getAllTypesOfSchedule",{headers: this.headers_object })
  }

  public addEvent(idTeam: number,date: string,typeEventId: number){
    let id = "idTeam=" + idTeam;
    let dateUrl = "&date=" + date;
    let idEvent = "&typeEventId=" + typeEventId;
    return this.httpClient.post(this.basePath + "/addSchedule?" + id + dateUrl + idEvent,{},{headers: this.headers_object });
  }

  public deleteEvent(id: number){
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('key')
    })
    return this.httpClient.delete(this.basePath + "/deleteEventOfSchedule/" + id,{headers: this.headers_object });
  }

  public deletePlayer(idPlayer: number, idTeam: number){
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('key')
    })
    return this.httpClient.delete(this.basePath + "/deletePlayerOfTeam?idPlayer=" + idPlayer + "&idTeam=" + idTeam,{headers: this.headers_object });
  }

  public changeStatistic(idPlayer: number,training: number, match: number, assists: number, goals: number){
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('key')
    })
    return this.httpClient.put(this.basePath + "/changePlayerStatistics?idPlayer=" + idPlayer + "&training="+training+"&match="+match+"&assists="+assists+"&goals="+goals, {},{headers: this.headers_object });
  }


}

export interface Schedule {
  idSchedule: number;
  idTeam: number;
  nameTeam: string;
  date: string,
  nameEvent: string;
}

export interface EventsName {
  idEvent: number;
  name: string;
}

export interface Statistic {
  training: number;
  matches: number;
  goals: number;
  assist: number;
  idStatistic: number;
}

export interface Players {
  idPlayer: number;
  name: string;
  familyName: string;
  age: number;
  team: Team[];
  statistics: Statistic[];
}

export interface Team {
  teamId: number;
  teamName: string;
  numberOfPlayers: number;
}

