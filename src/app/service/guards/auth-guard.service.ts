import { Injectable } from '@angular/core';
import {AuthorizationServiceService} from "../authorization/authorization-service.service";
import {CanActivate} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(
    public authService: AuthorizationServiceService
  ) {
  }

  canActivate(): boolean {
    return this.authService.isAuthenticated();
  }
}
