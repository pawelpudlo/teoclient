import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  message:string = "";
  name:string = "";
  familyName:string = "";
  idTeam!: number;
  idTypeEvent!: number;
  idSchedule!: number;
  training!: number;
  match!: number;
  goal!: number;
  assist!: number
  key: string = "";

  constructor() { }

  setMessage(data: string){
    this.message=data;
  }

  getMessage(){
    return this.message;
  }

  setMoreMessage(data: string,name: string, familyName: string,training: number,match: number, goal: number, assist: number){
    this.message = data;
    this.name = name;
    this.familyName = familyName;
  }

  getMoreMessage(){
    return [this.message,this.name,this.familyName,this.training,this.match,this.goal,this.assist];
  }

  setKey(key: string){
    this.key = key;
  }

  getKey(){
    return this.key;
  }

}
