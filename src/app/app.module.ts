import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { BlogComponent } from './components/blog/blog.component';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TeamsComponent } from './components/cards/teams/teams.component';
import {HttpClientModule} from "@angular/common/http";
import { AddTeamComponent } from './components/cards/add-team/add-team.component';
import {FormsModule} from "@angular/forms";
import { TeamComponent } from './components/cards/team/team.component';
import { AddPlayerComponent } from './components/cards/add-player/add-player.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { AddEventComponent } from './components/cards/add-event/add-event.component';
import { EditStatisticComponent } from './components/cards/edit-statistic/edit-statistic.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';

// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BlogComponent,
    NavbarComponent,
    TeamsComponent,
    AddTeamComponent,
    TeamComponent,
    AddPlayerComponent,
    ScheduleComponent,
    AddEventComponent,
    EditStatisticComponent,
    LoginComponent,
    SignupComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
