import {Component, Input, OnInit} from '@angular/core';
import {Team, TeamService} from "../../../service/team.service";
import {SharedService} from "../../../shared/shared.service";


@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {

  public teams: Team[] = [];
  message: string = "";

  constructor(private teamService: TeamService,public shared:SharedService) { }

  ngOnInit(): void {
    this.teamService.getTeams().subscribe(response =>{
      this.teams = response;
    });
  }

  createSharedMessage(id: number): void{
    this.message = id + "";
    this.shared.setMessage(this.message)
  }



}
