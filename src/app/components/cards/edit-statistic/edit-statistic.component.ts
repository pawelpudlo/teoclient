import { Component, OnInit } from '@angular/core';
import {TeamService} from "../../../service/team.service";
import {SharedService} from "../../../shared/shared.service";

@Component({
  selector: 'app-edit-statistic',
  templateUrl: './edit-statistic.component.html',
  styleUrls: ['./edit-statistic.component.css']
})
export class EditStatisticComponent implements OnInit {

  idPlayer!: number;
  training!: number;
  match!: number;
  goal!: number;
  assist!: number;
  message: string = "";
  name: string = "";
  familyName: string = "";
  t: string = "";
  m: string = "";
  g: string = "";
  a: string = "";


  constructor(private teamService: TeamService,private shared: SharedService) { }


  ngOnInit(): void {
    this.idPlayer = Number(this.shared.getMoreMessage()[0]);
    this.name = "" + this.shared.getMoreMessage()[1];
    this.familyName = "" + this.shared.getMoreMessage()[2];
    this.t = "" + this.shared.getMoreMessage()[3];
    this.m = "" + this.shared.getMoreMessage()[4];
    this.g = "" + this.shared.getMoreMessage()[5];
    this.a = "" + this.shared.getMoreMessage()[6];
  }

  changeStatistic(){
    this.teamService.changeStatistic(this.idPlayer,this.training,this.match,this.assist,this.goal).subscribe(response =>{
      this.message = "YOU UPDATE STATISTIC/ ZAKTUALIZOWANO STATYSTYKI"
    },error => {
      this.message = "ERROR / BŁAD AKTUALIZACJI"
    })
  }

}
