import { Component, OnInit } from '@angular/core';
import {EventsName, Team, TeamService} from "../../../service/team.service";

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.css']
})
export class AddEventComponent implements OnInit {

  dateFormat: string = "";
  information: Object = ""
  idTeam!: number;
  date: string = "";
  typeEventId!: number;
  teams: Team[] = [];
  public eventsName: EventsName[] = [];

  yearList = ['2022','2023','2024','2025','2026','2027','2028','2029'];
  monthList = ['01','02','03','04','05','06','07','08','09','10','11','12'];
  dayList = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
  hourList = ['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'];
  minList = ['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60'];

  year: String = "";
  month: String = "";
  day: String = "";
  hour: String = "";
  min: String = "";
  sec: String = "";


  constructor(private teamService: TeamService) { }

  ngOnInit(): void {
    this.teamService.getTeams().subscribe(response =>{
      this.teams = response;
    });

    this.teamService.getAllNameEvents().subscribe(response =>{
      this.eventsName = response;
    });
  }

  addEvent(){

    this.dateFormat = this.year + "-" + this.month + "-" + this.day + "T" + this.hour + ":" + this.month + ":" + this.day + "-02:00";


    this.teamService.addEvent(this.idTeam,this.dateFormat,this.typeEventId).subscribe(response =>{
      this.information = "You add event"
    },error => {
      this.information = "Bad information";
    });
  }

}
