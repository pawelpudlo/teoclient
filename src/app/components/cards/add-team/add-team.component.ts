import { Component, OnInit } from '@angular/core';
import {Team, TeamService} from "../../../service/team.service";

@Component({
  selector: 'app-add-team',
  templateUrl: './add-team.component.html',
  styleUrls: ['./add-team.component.css']
})
export class AddTeamComponent implements OnInit {

  teamName = "";
  information: Object = "";

  constructor(private teamService: TeamService) { }

  ngOnInit(): void {
  }

  addTeam(teamName: string): void {
    console.log(teamName)
    this.teamService.addTeam(teamName).subscribe(response =>{
      console.log(response);
      this.information = "You Add Team";

    }, error => {
      console.log(error);
      this.information = "ERROR";
    });
  }


}
