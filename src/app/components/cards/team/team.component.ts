import { Component, OnInit } from '@angular/core';
import {Players, Team, TeamService} from "../../../service/team.service";
import {ActivatedRoute} from "@angular/router";
import {SharedService} from "../../../shared/shared.service";

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  public players: Players[] = [];
  message: string = "";
  id!: number;
  info: string = "";

  constructor(private teamService: TeamService,private shared: SharedService) { }

  ngOnInit(): void {

    this.message = this.shared.getMessage();

    this.id = Number(this.message);

    this.teamService.getAllPlayersByTeam(this.id).subscribe(response => {
      this.players = response;
    }, error => {
      console.log(error);
    })
  }

  deletePlayer(idPlayer: number): void{
    this.teamService.deletePlayer(idPlayer,this.id).subscribe(response=>{
      this.info = "YOU DELETE PLAYER / ZAWODNIK USUNIĘTY";
    },error => {
      this.info = "ERROR DELETE PLAYER / BŁĄD USUWANIA ZAWODNIKA"
    })
  }

  createSharedMoreMessage(id: number,name: string, fname: string,t: number,m: number,g: number,a: number): void{
    this.message = id + "";
    this.shared.setMoreMessage(this.message,name,fname,t,m,g,a);
  }

  createSharedMessage(id: number): void{
    this.message = id + "";
    this.shared.setMessage(this.message)
  }

}
