import { Component, OnInit } from '@angular/core';
import {TeamService} from "../../../service/team.service";
import {SharedService} from "../../../shared/shared.service";

@Component({
  selector: 'app-add-player',
  templateUrl: './add-player.component.html',
  styleUrls: ['./add-player.component.css']
})
export class AddPlayerComponent implements OnInit {

  name = "";
  familyName = "";
  age= "";
  idTeam!: number;
  information: Object = "";
  message: string = "";

  constructor(private teamService: TeamService,private shared: SharedService) { }

  ngOnInit(): void {

    this.message = this.shared.getMessage();

    this.idTeam = Number(this.message);

  }

  addPlayer(name: string, familyName: string, age: string,idTeam: number): void {
    this.teamService.addPlayer(name,familyName,age,idTeam).subscribe(response =>{
      console.log(response);
      this.information = "Dodałeś zawodnika";
    }, error => {
      console.log(error);
      this.information = "Błąd podczas dodania zawodnika";
    });
  }

}
