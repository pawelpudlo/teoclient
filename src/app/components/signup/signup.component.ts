import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {AuthorizationServiceService} from "../../service/authorization/authorization-service.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(public authorizationService: AuthorizationServiceService) { }

  ngOnInit(): void {
  }

  onSubmit(signInForm: NgForm){
    console.log(signInForm.value.login)
    this.authorizationService.registration(signInForm.value.login,signInForm.value.password).subscribe(response =>{
      console.log(signInForm.value.login+ "-" +signInForm.value.password)
    },error => {
    })
  }

}
