import { Component, OnInit } from '@angular/core';
import {AuthorizationServiceService} from "../../service/authorization/authorization-service.service";

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  status: boolean = false;

  constructor(public authorizationService: AuthorizationServiceService) { }

  ngOnInit(): void {
  }

}


