import { Component, OnInit } from '@angular/core';
import {EventsName, Schedule, Team, TeamService} from "../../service/team.service";
import {SharedService} from "../../shared/shared.service";

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  public schedules: Schedule[] = [];
  public eventsName: EventsName[] = [];
  public teams: Team[] = [];
  public type: string = "";
  public idTeam : number = 0;
  public info: string = "";

  constructor(private teamService: TeamService,private shared: SharedService){}

  ngOnInit(): void {
    this.teamService.getTeams().subscribe(response =>{
      this.teams = response;
    });

    this.teamService.getAllNameEvents().subscribe(response =>{
      this.eventsName = response;
    });
  }

  showTeam(): void{
    this.teamService.getAllScheduleByIdTeam(this.idTeam).subscribe(response =>{
      this.schedules = response;
    });
  }

  deleteEvent(id: number): void{
    this.teamService.deleteEvent(id).subscribe(response =>{
      this.info = "YOU DELETE EVENT / USUNĄŁEŚ WYDARZENIE"
    }, error => {
      this.info = "DELETE ERROR / BŁĄD USUWANIA"
    });
  }

}
