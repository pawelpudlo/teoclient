import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthorizationServiceService} from "../../service/authorization/authorization-service.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public authorizationService: AuthorizationServiceService) { }

  ngOnInit(): void {
  }

  onSubmit(signInForm: NgForm){
    console.log(signInForm.value.login)
    this.authorizationService.authenticate(signInForm.value.login,signInForm.value.password)
}

}
